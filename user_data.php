<?php
    function validate_text($fieldName){
        return isset($_POST[$fieldName]) && !empty(trim($_POST[$fieldName])) ? $_POST[$fieldName] : false;
    }
    
    function validate_email($fieldName){
        return filter_var($_POST[$fieldName], FILTER_VALIDATE_EMAIL) ? $_POST[$fieldName] : false;
    }

    $data_file = fopen('data.txt', 'a');
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $first_name = validate_text('first_name');
        $last_name = validate_text('last_name');
        $email = validate_email('email');
        
        if (fwrite($data_file, "$first_name, $last_name, $email \n")) {
            echo 'Данные успешно записаны!';
        } else {
            echo 'Произошла какая-то ошибка';
        }
    }

    fclose($data_file);

?>